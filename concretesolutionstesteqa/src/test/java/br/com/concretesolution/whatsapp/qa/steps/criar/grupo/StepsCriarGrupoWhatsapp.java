package br.com.concretesolution.whatsapp.qa.steps.criar.grupo;

import java.awt.AWTException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import br.com.concretesolution.whatsapp.qa.constants.Constant;
import br.com.concretesolution.whatsapp.qa.fases.MenuPrincipal;
import br.com.concretesolution.whatsapp.qa.fases.Newgroup;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class StepsCriarGrupoWhatsapp {
	private MenuPrincipal menuPrincipal;
	private Newgroup newGroup;
	WebDriver driver;

	public void sobeNavegador() {
		driver = new FirefoxDriver();
	}

	@Given("estou na pagina principal whatsapp")
	public void acessarPaginaPrincipal() {
		sobeNavegador();
		driver.get(Constant.URLWHATSAPP);
	}

	@When("clico no botao menu")
	public void clicoNoBotaoMenu() throws AWTException {

		menuPrincipal =  PageFactory.initElements(driver,MenuPrincipal.class);
		
		menuPrincipal = menuPrincipal.esperarAutenticacaoCelular();
		menuPrincipal = menuPrincipal.clicaMenuPrincipal();
	}

	@And("clico na opcao new group")
	public void clicoNaOpcaoNewGroup() {

		newGroup = menuPrincipal.clicaMenuNewGroup();

	}

	@Given("^tela NewGroup carregada de forma correta onde informacoes \"(.*?)\" e \"(.*?)\"$")
	public void preenchendoInfoParaCadeGrupos(String nome, String nomeImagem) {

		newGroup.criarGrupo(nome, nomeImagem);
	}

}
