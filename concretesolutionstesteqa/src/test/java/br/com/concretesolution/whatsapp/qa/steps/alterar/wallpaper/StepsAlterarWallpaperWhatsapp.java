package br.com.concretesolution.whatsapp.qa.steps.alterar.wallpaper;

import java.awt.AWTException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import br.com.concretesolution.whatsapp.qa.constants.Constant;
import br.com.concretesolution.whatsapp.qa.fases.MenuPrincipal;
import br.com.concretesolution.whatsapp.qa.fases.Settings;
import br.com.concretesolution.whatsapp.qa.fases.Wallpaper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepsAlterarWallpaperWhatsapp {
	private MenuPrincipal menuPrincipal;
	private Settings settings;
	private Wallpaper wallpaper;
	WebDriver driver;

	public void sobeNavegador() {
		driver = new FirefoxDriver();
	}

	@Given("estou na pagina principal whatsapp")
	public void acessarPaginaPrincipal() {
		sobeNavegador();
		driver.get(Constant.URLWHATSAPP);
	}

	@When("clico no botao menu")
	public void clicoNoBotaoMenu() throws AWTException {

		menuPrincipal =  PageFactory.initElements(driver,MenuPrincipal.class);
		
		menuPrincipal = menuPrincipal.esperarAutenticacaoCelular();
		menuPrincipal = menuPrincipal.clicaMenuPrincipal();
	}

	@And("clico na opacao settings")
	public void clicaNaOpcaoStting(){
		settings = menuPrincipal.clicaBtnSettings();
	}
	
	@When("tela settings carregada de forma correta e clico chat wallpaper")
	 public void clicarMenuWallpaper(){
		 wallpaper = settings.clicaBtnChatWallpaper();
	}
	
	@Then("seleciono uma cor qualquer azul")
	private void selectCor(){
		wallpaper.selectCorFixadaAzul();
	}

	
}
