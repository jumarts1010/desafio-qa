package br.com.concretesolution.whatsapp.qa.fases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import br.com.concretesolution.qa.utils.SalvarEvidencia;

public class Wallpaper {

	@FindBy(xpath="//div[@id='app']/div/div/div/span/div/span/div/div/div/span[29]")
	private WebElement selectCorAzul;
	
	WebDriver driver;
	
	public Wallpaper(WebDriver driver) {
	  this.driver = driver;
	}
	
	public void selectCorFixadaAzul(){
		driver.switchTo().activeElement();
		selectCorAzul.click();
		SalvarEvidencia.evidenciar(driver,"Alteracao Cor");
	}
	
	
}
