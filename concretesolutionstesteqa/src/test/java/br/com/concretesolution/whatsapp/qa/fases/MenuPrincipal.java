package br.com.concretesolution.whatsapp.qa.fases;

import java.awt.AWTException;
import java.awt.Robot;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.concretesolution.qa.utils.SalvarEvidencia;
import br.com.concretesolution.qa.utils.Utils;

/**
 * 
 * @author Julio Cesar Classe Responsavel Pelo Mapeamento de Objetos Menu
 *         Principal
 */
public class MenuPrincipal {

	@FindBy(xpath = "/html/body/div/div/div/div[2]/header/div[2]/div/span/div[2]/button")
	private WebElement btnMenu;

	@FindBy(css = "html.js.cssanimations.csstransitions.no-webp body div#app div.app-wrapper.app-wrapper-web.app-wrapper-main div.app.two div#side.pane.pane-list.pane-one header.pane-header.pane-list-header div.pane-list-user div.avatar.icon-user-default div.avatar-body img.avatar-image.is-loaded")
	private WebElement identificacao;

	@FindBy(css = "a.ellipsify")
	private WebElement btnMenuNewGroup;
	
	@FindBy(xpath="//a[contains(text(),'Settings')]")
    private WebElement btnSetting;
	
	final WebDriver driver;

	public MenuPrincipal(WebDriver driver) {
		this.driver = driver;
	}
	
	

	public MenuPrincipal esperarAutenticacaoCelular() throws AWTException {
		while (false==Utils.isElementPresent(identificacao, driver)) {
		    JOptionPane.showMessageDialog(null, "Solicitar Acesso chave Cod no seu Aplicativo no Celular !!");	
			Robot robot = new Robot();
			robot.delay(10000);
		}
		return PageFactory.initElements(driver, MenuPrincipal.class);
	}

	public MenuPrincipal clicaMenuPrincipal() {
		btnMenu.click();
		return PageFactory.initElements(driver, MenuPrincipal.class);
	}

	
	public Settings clicaBtnSettings(){
		btnSetting.click();
		return PageFactory.initElements(driver,Settings.class);
	}
	
	public Newgroup clicaMenuNewGroup() {
		btnMenuNewGroup.click();
		SalvarEvidencia.evidenciar(driver, "Evidencia Menu Principal");
		return PageFactory.initElements(driver, Newgroup.class);
	}

}
