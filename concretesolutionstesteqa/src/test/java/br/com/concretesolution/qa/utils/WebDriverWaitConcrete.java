package br.com.concretesolution.qa.utils;

import java.lang.reflect.Field;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class WebDriverWaitConcrete {

	public static void aguardarTodosElementosDaPagina(Object classePageObject, WebDriver driver) throws IllegalArgumentException, IllegalAccessException {
		Class<?> classe = classePageObject.getClass();
		for (Field atributo : classe.getDeclaredFields()) {
			if(atributo.getType().toString().contains("WebElement")) {
				atributo.setAccessible(true);
				WebElement element = (WebElement) atributo.get(classePageObject);
				aguardarEvento(element, driver);
			}
		}
	}

	public static void aguardarEvento(WebElement element, WebDriver driver) {
		for (int i = 0; i <= 40; i++) {
			try {
				Thread.sleep(1100);
			} catch (InterruptedException e) {
				System.err.println("Thread Sleep falhou");
				e.printStackTrace();
			}
			if (element.isDisplayed() && element.isEnabled()) {
				break;
			}
		}
	}
}