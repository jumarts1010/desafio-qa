package br.com.concretesolution.qa.testes;


import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:cenarios", tags = "@CriarGrupoWhatsapp", 
glue = "br.com.concretesolution.whatsapp.qa.steps.criar.grupo", monochrome = true, dryRun = false)
public class CriarGrupoWhatsapp {
	

}
