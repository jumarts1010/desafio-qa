package br.com.concretesolution.qa.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Utils {
	private final static String caminho = System.getProperty("user.dir").concat("\\src\\test\\resources\\");
    
	public static void deleteCookies(WebDriver driver) {
		driver.manage().deleteAllCookies();
	}

	public static void validarAlert(String alertMessage, WebDriver driver) {
		for (int i = 0; i <= 8; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.err.println("Thread Sleep falhou");
				e.printStackTrace();
			}
			if (isAlertPresent(driver)) {
				String txtAlert = driver.switchTo().alert().getText().trim();
				if (!alertMessage.trim().equals(txtAlert)) {
					System.err.println("ALERT: " + alertMessage
							+ " N�O LOCALIZADO");
					System.err.println("ALERT PRESENTE: " + txtAlert);
				}
				driver.switchTo().alert().accept();
				return;
			}
		}
	}

	public static boolean isAlertPresent(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	public static String getTextAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		return alertText;
	}

	public static boolean isElementPresent(By by, WebDriver driver) {
		try {
			driver.findElement(by);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isElementPresent(WebElement element, WebDriver driver) {
		try {
			element.getTagName();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
			
		
	public static void selectMenuItemEqualsIgnoresCase(WebDriver driver, WebElement element, String item) {

		try {
			Thread.sleep(2000);
			element.click();
			element.sendKeys(item);
			element.sendKeys(Keys.TAB);
			Thread.sleep(1000);	   

		} catch (Exception e) {
			throw new IllegalArgumentException("N�o foi localizado valor: " + item);
		}
	}

	public static void digitarMenuItemEqualsIgnoresCase(WebDriver driver, WebElement element, String item) {

		try {
			Thread.sleep(2000);
			element.findElement(By.xpath("div[1]/input")).click();
			element.findElement(By.xpath("div[1]/input")).sendKeys(item);

			for(WebElement opcoes : element.findElements(By.xpath("div[2]/div/div/div"))) {
				try{
					if(opcoes.findElement(By.xpath("div/div/span/span")).getText().equalsIgnoreCase(item)) {
						opcoes.findElement(By.xpath("div/div/span/span")).click();
						break;
					}
				}catch (NoSuchElementException suchElementException) {
					continue;
				}
			}

			Thread.sleep(1000);	   

		} catch (Exception e) {
			throw new IllegalArgumentException("N�o foi localizado valor: " + item);
		}
	}

	public static void selectAngularMenuItemEqualsIgnoresCase(WebDriver driver, WebElement element, WebElement elementTeste, String item) {
		try {
			Thread.sleep(2000);
			element.click();
			elementTeste.sendKeys(item);
			elementTeste.sendKeys(Keys.TAB);
			Thread.sleep(3000);	   

		} catch (Exception e) {
			throw new IllegalArgumentException("N�o foi localizado valor: " + item);
		}
	}

	public static void indexarDocSemSikule(String nomeDocumento){
		try {
			Robot robot = new Robot();
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			String text = caminho +nomeDocumento;
			StringSelection texto = new StringSelection(text);
			clipboard.setContents(texto, null);
			Thread.sleep(3000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(5000);
		} catch (AWTException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void escDialogoProxy(){
		try {
			Robot robot = new Robot();
			robot.delay(7000);
			robot.keyPress(KeyEvent.VK_ALT);
			robot.delay(200);
			robot.keyPress(KeyEvent.VK_F4);
			robot.keyRelease(KeyEvent.VK_F4);
			robot.keyRelease(KeyEvent.VK_ALT);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	
	}

	public static void telaFull() throws AWTException {
		Robot robot = new Robot();
		robot.delay(5000);
		robot.keyPress(122);
		robot.keyRelease(122);
	}

	public static void telaNormal() throws AWTException {
		Robot robot = new Robot();
		robot.delay(5000);
		robot.keyPress(122);
		robot.keyRelease(122);
	}
}
